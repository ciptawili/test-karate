# Testing API using Karate

## Requirement
* ###Java Development Kit
  ###Installation
    * Download and install JDK
      Setting the system variables 
    * Create and open your .bash_profile using Terminal
      ``` 
        $ cd ~/
        $ touch .bash_profile
        $ open -e .bash_profile
      ```
    * Set the Java paths in your `.bash_profile` file
      ```
      export JAVA_HOME=/Library/Java/JavaVirtualMachines/{JDK_VERSION_FOLDER}/Contents/Home
      export PATH=$PATH:$JAVA_HOME/bin
      ```

    * Save and exit your .bash_profile
    * Type java -version in your Terminal to verify installation and find out your java version
    * See the output of java version
  
* ###Maven
  ###Installation
    * Download Maven as your build tools
    * Extract downloaded maven into your current or random directory
    * Adding maven to the environment path
    * Add path variable to .bash_profile file
      ```
      export HOME=/{YOUR_DIRECTORY}/apache-maven-{YOURVERSION}
      export PATH=$PATH:$HOME/bin
      ```

    * Verify your maven installation using mvn -version in your Terminal
    * See the output of Maven version


## Running tests
* Run all tests on stage with parallel mode
    ```
    mvn clean test -Dtest=testParallel
    ```
* Run spesific tests feature on stage with parallel mode
    ```
    mvn clean test -Dkarate.options="--tags @tag" -Dtest=testParallel
    ```  

## Test Report
* Test report automatically generated on `target` folder after finished the test execution
* Open `overview-features.html` file on your browser from `target/cucumber-reports/advanced-reports/cucumber-html-reports` folder
