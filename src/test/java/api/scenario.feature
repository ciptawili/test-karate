Feature: test

  Background:
    * url testUrl

  Scenario: Open Home Page
    When method get
    Then status 200
  @login
  Scenario: Login With Valid Data
    Given path 'login'
    And form field username = 'root'
    And form field password = 'root123'
    When method post
    Then status 200

  Scenario: Open Home Page After Login
    * def login = call read('scenario.feature@login')
    Given path 'login'
    And method get
    Then status 302

  Scenario: Logout
    Given path 'logout'
    When method post
    Then status 200

  Scenario: Open Data Page Before Login
    Given path 'data'
    And method get
    Then status 302

  Scenario: Open Data Page After Login
    * def responseData =
    """
    <h1>Pemasukan</h1><table><tr><th>TimeStamp</th><th>Deskripsi</th><th>Jumlah</th></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pemasukan10</td><td>10</td></tr></table><h1>Pengeluaran</h1><table><tr><th>TimeStamp</th><th>Deskripsi</th><th>Jumlah</th></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr><tr><td>2018-07-10</td><td>pengeluaran10</td><td>10</td></tr></table><br><html>
    <head>
    <title></title>
    </head>
    <body>
        <form action="/filter" method="post">
            Start:<input type="text" name="start">
            End:<input type="text" name="end">
            <input type="submit" value="Filter">
        </form>
        Date Format: yyyy-mm-dd
    </body>
    </html>
    """
    * path 'login'
    * form field username = 'root'
    * form field password = 'root123'
    * method post
    Given path 'data'
    And method get
    Then status 200
    *  match response contains any response data

  Scenario: Filter Data With Valid Parameter
    * path 'login'
    * form field username = 'root'
    * form field password = 'root123'
    * method post
    Given path 'data'
    And method get
    When path 'filter'
    * form field start = '2017-01-01'
    * form field end = '2018-01-01'
    And method post
    Then status 200

  Scenario: Filter Data With Invalid Parameter
    * path 'login'
    * form field username = 'root'
    * form field password = 'root123'
    * method post
    Given path 'data'
    And method get
    When path 'filter'
    * form field start = '2018-01-01'
    * form field end = '2017-01-01'
    And method post
    Then status 405

